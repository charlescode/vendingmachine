<?php
namespace VendingMachine;
require_once('Factory.php');
require_once('BankCard.php');
require_once('VendingMachine.php');


$factory = new Factory();
$card = $factory->make('card');

$stock = [
        'A1' => ['cock', 'cock','cock','cock','cock'],
        'A2' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
        'A3' =>  ['purewater', 'purewater', 'purewater', 'purewater', 'purewater',],
        'A4' =>  ['fanta', 'fanta', 'fanta', 'fanta', 'fanta',],
        'D1' => ['red bull' , 'red bull' , 'red bull' , 'red bull' , 'red bull' ,],
        'D2' => ['mineralwater', 'mineralwater', 'mineralwater', 'mineralwater', 'mineralwater'],
        'C3' => ['cock', 'cock','cock','cock','cock'],
        'B4' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
    ];
//the array shows drink price in each position.
$panelKeyCode = [
     'A1' => '4.5', 'A2' => '4.2', 'A3' => '5.5', 'A4' => '6', 'A5'=>'5.8',
     'B1'=> '3', 'B2'=>'3.2', 'B3'=>'3.4', 'B4' => '3.6', 'B5'=>'4',
     'C1' => '3.2', 'C2' => '3.6', 'C3'=>'3.8', 'C4'=>'2.5', 'C5'=>'5.2',
     'D1'=>'5.2', 'D2' => '5.4', 'D3' => '5.6', 'D4'=>'6.2', 'D5'=>'5.5',
    ];
$machine = $factory->make('machine', $stock, $panelKeyCode);

// var_dump($machine->getStock());

try {
    $machine->getPrice('A1');
    $machine->payByCash('A1');
    $summary = $machine->soldSummary();
} catch(Exception $e) {
    echo $e->getMessage();
}
// echo "summary is $summary<br>";
var_dump($summary);

$machine->insertMoney(2.5, 'coin');
$machine->insertMoney(10, 'note');
$money = $machine->getCredit();
echo "money is $money<br>";
$machine->getPrice('A1');
$returnChange = $machine->payByCash('A1');
$summary = $machine->soldSummary();
// echo "return change is $returnChange<br>";
// echo 'hello world, hello Huawei';
var_dump($returnChange);
var_dump($summary);

$machine->insertMoney(3.5, 'coin');
$machine->getPrice('D2');
$returnChange = $machine->payByCard($card, 'D2');
$summary = $machine->soldSummary();
// echo "return change is $returnChange<br>";
var_dump($returnChange);
var_dump($summary);

$fill =  [
        'A1' => ['cock', 'cock','cock','cock','cock'],
        'A5' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
        'D4' =>  ['purewater', 'purewater', 'purewater', 'purewater', 'purewater',],
    ];
try{
    $machine->fillStock($fill);
} catch (Exception $e){
    echo $e->getMessage();
}
var_dump($machine->getStock());


$machine->insertMoney(2.5, 'coin');
$machine->insertMoney(5, 'note');
$money = $machine->getCredit();
echo "money is $money<br>";
$machine->getPrice('D2');
$returnChange = $machine->payByCash('D2');
$summary = $machine->soldSummary();
// echo "return change is $returnChange<br>";
// echo 'hello world, hello Huawei';
var_dump($returnChange);
var_dump($summary);
var_dump($machine->getStock());

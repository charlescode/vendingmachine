<?php
namespace VendingMachine;

class Factory
{
    public function make($type, $stock = [], $keyCode = [])
    {
        switch($type) {
            case 'card':
                $obj = new BankCard();
                break;
            case 'machine':
                $obj = new VendingMachine($stock, $keyCode);
                break;
        }

        return $obj;
    }
}

<?php
namespace VendingMachine;

class VendingMachine
{
    protected $stock;
    protected $panelKeyCode;
    protected $coin = 0;
    protected $note = 0;
    protected $credit =0 ;
    protected $totalSummary = 0 ;
    protected $sold = [];

    /**
     * initial the machine stock, such as refreshment, drinks,
     * also set product position and price.
     */
    function __construct($stock = [], $panelKeyCode = [])
    {
        //set machine product.
        $this->stock = $stock;
        //set product position and price.
        $this->panelKeyCode = $panelKeyCode;

    }

    /**
     *fill in $stock
     */
    public function fillStock($fill = [])
    {
        foreach ($fill as $key => $value) {
            if (!array_key_exists($key, $this->panelKeyCode)) {
                throw new \Exception("$key postion does not exist in machin position");
            }
            if (isset($this->stock[$key])) {
                $this->stock[$key] = array_merge($this->stock[$key], $fill[$key]);
            } else {
                $this->stock[$key] = $fill[$key];
            }
        }
    }


    /**
     * 3 types of payment, 1 note, 2 coin, 3 buy by card.
     *
     */
    public function payByCash(string $key)
    {
        $returnChange = 0;
        try {
                $charged = $this->getPrice($key) ;
                $credit = $this->getCredit();
                if ($credit < $charged) {
                    throw new \Exception('Not have enough money to buy product');
                }
                $returnChange = $this->transaction($key, $charged, $credit);
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
        //return money only can return coin.
        return $returnChange;
    }

    public function payByCard(BankCard $card, string $key)
    {
        $returnChange = 0;
        try {
            $charged = $this->getPrice($key);
            $balance = $charged - $this->credit <= 0? 0 : $charged - $this->credit;
            if ($balance > 0) {
                $card->payment($balance);
            }
            $returnChange = $this->transaction($key, $charged, $this->credit, $balance);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return $returnChange;
    }

    /**
     * key is product position.
     * charge is how much need $charge
     * credit is how much money as credit in the machine.
     */
    public function transaction(string $key,float $charge, float $credit, $balance = 0)
    {
        $returnChange = $this->returnChange($charge, $credit, $balance);
        $this->totalSummary += $charge;
        //clear cash money.
        $this->note = 0;
        $this->coin = 0;
        $this->credit = 0;
        //product quality reduce by one.
        array_shift($this->stock[$key]);
        if (isset($this->sold[$key])) {
            $this->sold[$key]++;
        } else {
            $this->sold[$key] = 1;
        }
        return $returnChange;
    }


    /**
     * when buy the product, return 5 types of coins[2,1, 50c. 20c, 10c], calculate how many kinds of coin will return.
     */
    public function returnChange(float $charge, float $credit, $balance = 0)
    {
        $changeResult = '';
        $change = $credit - $charge + $balance;
        if ($change >= 2) {
            $twoDollarCoin = floor($change / 2);
            $change = $change - 2 * $twoDollarCoin;
            $changeResult .= "change $twoDollarCoin two dollar coin, ";
        }
        if ($change >= 1) {
            $oneDollarCoin = floor($change / 1);
            $change = $change - 1 * $oneDollarCoin;
            $changeResult .= "change $oneDollarCoin one dollar coin, ";
        }
        if ($change >= 0.5) {
            $fiftyCentCoin = floor($change / 0.5);
            $change = $change - 0.5 * $fiftyCentCoin;
            $changeResult .= "change $fiftyCentCoin fifty cent coin, ";
        }
        if ($change >= 0.2) {
            $twentyCentCoin = floor($change / 0.2);
            $change = $change - 0.2 * $twentyCentCoin;
            $changeResult .= "change $twentyCentCoin twenty cent coin, ";
        }
        if (abs($change - 0.1)<=0.01) {
            $changeResult .= "change 1 ten cent coin ";
        }
        return [
                'coin' => $credit - $charge + $balance,
                'description' => $changeResult,
            ];
    }

    /**
     * refund money, not buy anything, then return coin.
     */
     public function refund()
     {
         $this->coin = 0;
         $this->note = 0;
         $this->credit = 0;
     }

     public function getCredit()
     {
            return $this->credit;
     }

     /**
     * insert coin or  note
     */
     public function insertMoney(float $money , $type = 'coin')
     {
         if ($this->checkMoney($money) === false) {
             throw new \Exception('Find fake money, please check');
         }
         if (strtolower($type) === 'coin') {
             $this->coin += $money;
         }
         if (strtolower($type) == 'note') {
             $this->note += $money;
         }
         $this->credit = $this->coin + $this->note;
     }

     /**
      * after insert money, then check the money, find fake money or right money,
      * at the moment return true.
      */
      public function checkMoney(float $money)
      {
          return true;
          // return false;
      }

      /**
      *get input key postion product price
      */
      public function getPrice(string $key)
      {
          if (!array_key_exists($key, $this->panelKeyCode)) {
              throw new \Exception('The input position is not right');
          }
          if (!isset($this->stock[$key])) {
              throw new \Exception('No stock left in the position');
          }
          $price = $this->panelKeyCode[$key];
          return $price;
      }

     /**
      * summary machine sell information.
      */
     public function soldSummary()
     {
         return [
             'money' => $this->totalSummary,
             'products' => $this->sold
         ];
     }

     public function getStock()
     {
         return $this->stock;
     }


}

<?php
use PHPUnit\Framework\TestCase;
use VendingMachine\Bankcard;
use VendingMachine\VendingMachine;

require_once('BankCard.php');
require_once('VendingMachine.php');

class VendingMachineTest extends TestCase
{
    public function setUp()
    {

    }

    public function testVendingMachine()
    {
        $stock = [
                'A1' => ['cock', 'cock','cock','cock','cock'],
                'A2' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
                'A3' =>  ['purewater', 'purewater', 'purewater', 'purewater', 'purewater',],
                'A4' =>  ['fanta', 'fanta', 'fanta', 'fanta', 'fanta',],
                'D1' => ['red bull' , 'red bull' , 'red bull' , 'red bull' , 'red bull' ,],
                'D2' => ['mineralwater', 'mineralwater', 'mineralwater', 'mineralwater', 'mineralwater'],
                'C3' => ['cock', 'cock','cock','cock','cock'],
                'B4' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
            ];
        //the array shows drink price in each position.
        $panelKeyCode = [
             'A1' => '4.5', 'A2' => '4.2', 'A3' => '5.5', 'A4' => '6', 'A5'=>'5.8',
             'B1'=> '3', 'B2'=>'3.2', 'B3'=>'3.4', 'B4' => '3.6', 'B5'=>'4',
             'C1' => '3.2', 'C2' => '3.6', 'C3'=>'3.8', 'C4'=>'2.5', 'C5'=>'5.2',
             'D1'=>'5.2', 'D2' => '5.4', 'D3' => '5.6', 'D4'=>'6.2', 'D5'=>'5.5',
            ];

        $machine = new VendingMachine($stock, $panelKeyCode);
        $this->assertInstanceof(VendingMachine::class, $machine);

        $machine->insertMoney(5, 'note');
        $machine->insertMoney(2.2, 'coin');
        $credit = $machine->getCredit();
        $this->assertEquals($credit, 7.2);
        $price = $machine->getPrice('A1');
        $this->assertEquals($price, 4.5);
        $returnChange = $machine->payByCash('A1');
        $this->assertEquals($returnChange['coin'], 2.7);
        $this->assertContains('change 1 two dollar coin, change 1 fifty cent coin, change 1 twenty cent coin', $returnChange['description']);

        $machine->insertMoney(10, 'note');
        $returnChange = $machine->payByCash('D1');
        $this->assertEquals(9.7, $machine->soldSummary()['money']);
        $this->assertEquals(4.8, $returnChange['coin']);

        $card = new BankCard();
        $returnChange = $machine->payByCard($card, 'C3');
        $this->assertEquals(0, $returnChange['coin']);
        $this->assertEmpty($returnChange['description']);
        $this->assertEquals(5.2 + 3.8 + 4.5, $machine->soldSummary()['money']);
        $this->assertArraySubset(['A1' => 1,'D1' => 1,'C3' => 1], $machine->soldSummary()['products']);
        //buy C3 again
        $returnChange = $machine->payByCard($card, 'C3');
        $this->assertArraySubset(['A1'=> 1,'D1'=> 1,'C3'=> 2], $machine->soldSummary()['products']);

    }

    public function testAddStock()
    {
        $stock = [
                'A1' => ['cock', 'cock','cock','cock','cock'],
                'A2' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
                'A3' =>  ['purewater', 'purewater', 'purewater', 'purewater', 'purewater',],
                'A4' =>  ['fanta', 'fanta', 'fanta', 'fanta', 'fanta',],
                'D1' => ['red bull' , 'red bull' , 'red bull' , 'red bull' , 'red bull' ,],
                'D2' => ['mineralwater', 'mineralwater', 'mineralwater', 'mineralwater', 'mineralwater'],
                'C3' => ['cock', 'cock','cock','cock','cock'],
                'B4' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite',],
            ];
        //the array shows drink price in each position.
        $panelKeyCode = [
             'A1' => '4.5', 'A2' => '4.2', 'A3' => '5.5', 'A4' => '6', 'A5'=>'5.8',
             'B1'=> '3', 'B2'=>'3.2', 'B3'=>'3.4', 'B4' => '3.6', 'B5'=>'4',
             'C1' => '3.2', 'C2' => '3.6', 'C3'=>'3.8', 'C4'=>'2.5', 'C5'=>'5.2',
             'D1'=>'5.2', 'D2' => '5.4', 'D3' => '5.6', 'D4'=>'6.2', 'D5'=>'5.5',
            ];

        $machine = new VendingMachine($stock, $panelKeyCode);

        $fillStock = [
            'A2' =>  ['sprite', 'sprite', 'sprite', 'sprite'],
            'B3' =>  ['sprite', 'sprite', 'sprite', 'sprite', 'sprite'],
        ];
        $machine->fillStock($fillStock);
        $this->assertArrayHasKey('B3', $machine->getStock());
        $this->assertEquals(count($machine->getStock()['A2']), 9);
        $this->assertEquals(count($machine->getStock()['B3']), 5);
    }

    public function testBankCard()
    {
        $bankCard = new BankCard();
        $this->assertInstanceOf(BankCard::class, $bankCard);
    }

    public function tearDown()
    {

    }
}
